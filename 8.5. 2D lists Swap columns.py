#Dados dos enteros (el número de filas my columnas n de m × n 2d lista) y las siguientes m filas de n enteros, seguidas por dos enteros no negativos i y j menores que n, intercambie las columnas i y j de la lista 2d e imprime el resultado.

# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)

m, n = [int(s) for s in input().split()]
a = [[int(j) for j in input().split()] for i in range(m)]
col1, col2 = [int(s) for s in input().split()]
for i in range(m):
  a[i][col1], a[i][col2] = a[i][col2], a[i][col1]
for line in a:
  print(*line)