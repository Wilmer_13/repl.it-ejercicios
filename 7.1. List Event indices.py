#Dada una lista de números, encuentre e imprima todos sus elementos con índices pares (es decir, A [0] , A [2] , A [4] , ...).

a = [int(s) for s in input().split()]
print(a[0::2])