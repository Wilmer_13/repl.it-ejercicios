#Dada una lista de números con todos los elementos ordenados en orden ascendente, determine e imprima el número de elementos distintos que contiene.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

x = input()
list_x = list(map(int, x.split()))
total = 1

for i in range(1, len(list_x)):
    if list_x[i - 1] != list_x[i]:
        total += 1

print(total)