#Dado el texto: la primera línea contiene el número de líneas, luego las líneas de palabras. Imprima la palabra en el texto que ocurre con más frecuencia. Si hay muchas de esas palabras, imprima la que sea menor en orden alfabético.

# Read a string:
# s = input()
# Print a value:
# print(s)

contador = {}
for i in range(int(input())):
  palabras = input().split()
  for palabra in palabras:
    if palabra not in contador:
      contador[palabra] = 0
    contador[palabra] += 1
frecuencia = max(contador.values())
for palabra in sorted(contador):
  if contador[palabra] == frecuencia:
    print(palabra)
    break