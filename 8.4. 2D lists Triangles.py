##Dado un número entero n, cree una matriz bidimensional de tamaño n × n de acuerdo con las siguientes reglas e imprímalo:

#En el antidiagonal poner 1.
#En las diagonales arriba pone 0.
#En las diagonales debajo pone 2.

#Example input
#4

#Example output
#0 0 0 1
#0 0 1 2
#0 1 2 2
#1 2 2 2

# Read an integer:
# a = int(input())
# Print a value:
# print(a)

num = int(input())
a = [[0] * num for i in range(num)]
for i in range(num):
  for j in range(num):
    if i + j + 1 < num:
      a[i][j] = 0
    elif i + j + 1 == num:
      a[i][j] = 1
    else:
      a[i][j] = 2
for linea in a:
  print(*linea)
