#Dados dos enteros, el número de filas my columnas n de m × n 2d lista, y las siguientes m filas de n enteros, seguidos de un entero c . Multiplique cada elemento por c e imprima el resultado.

#Example input
#3 4
#11 12 13 14
#21 22 23 24
#31 32 33 34
#2

#Example output
#22 24 26 28
#42 44 46 48
#62 64 66 68

n, m = [int(i) for i in input().split()]
a = [[int(j) for j in input().split()] for i in range(n)]
rep = int(input())

for x in range(n):
  for y in range(m):
    a[x][y] = ((a[x][y]) * rep)

for row in a:
  print(' '.join([str(a) for a in row]))