#Dado un número entero n, cree una matriz bidimensional de tamaño n × n de acuerdo con las siguientes reglas e imprímalo:

#En la diagonal principal ponga 0.
#En las diagonales adyacentes a la principal pon 1.
#En las siguientes diagonales adyacentes ponga 2, y así sucesivamente.

# Read an integer:
# a = int(input())
# Print a value:
# print(a)

a = int(input())
primero = [i for i in range(a)]
despues = [[]]*a
for j in range(a):
  despues[j] = primero[j:0:-1]
  for l in primero[:a-j]:
    despues[j].append(l)
  print(*despues[j])