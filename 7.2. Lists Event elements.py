#Dada una lista de números, imprima todos sus elementos pares. Use un bucle for que itera sobre la lista misma y no sobre sus índices. Es decir, no use range ()

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = [int(s) for s in input().split()]
for elem in a:
    if elem % 2 == 0:
        print(elem)