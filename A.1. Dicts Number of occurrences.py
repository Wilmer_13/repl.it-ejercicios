#El texto se da en una sola línea. Para cada palabra del texto, cuente el número de veces que aparece antes.

# Read a string:
# s = input()
# Print a value:
# print(s)

texto = input().split()
tiempo = {}
for palabra in texto:
  if palabra not in tiempo:
    tiempo[palabra] = 0
  print(tiempo[palabra], end=' ')
  tiempo[palabra] += 1