#Dados dos enteros, el número de filas my columnas n de la lista m × n 2d, y las siguientes filas m de n enteros, encuentre el elemento máximo e imprima su número de fila y número de columna. Si hay muchos elementos máximos en diferentes filas, informe el que tenga un número de fila más pequeño. Si hay muchos elementos máximos en la misma fila, informe el que tenga un número de columna más pequeño.

# Read a 2D list of integers:
# a = [[int(j) for j in input().split()] for i in range(NUM_ROWS)]
# Print a value:
# print(a)

m, n = [int(s) for s in input().split()]
a = [[int(j) for j in input().split()] for i in range(m)]
max_value, max_i, max_j = a[0][0], 0, 0
for i in range(m):
  for j in range(n):
    if a[i][j] > max_value:
      max_value, max_i, max_j = a[i][j], i, j
print(max_i, max_j)