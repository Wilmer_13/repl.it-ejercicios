#Te dan un diccionario que consiste en pares de palabras. Cada palabra es sinónimo de la otra palabra en su par. Todas las palabras en el diccionario son diferentes.

#Después del diccionario hay una palabra más dada. Encuentra un sinónimo para ello.

# Read a string:
# s = input()
# Print a value:
# print(s)

sinonimos = {}
for i in range(int(input())):
  w1, w2 = input().split()
  sinonimos[w1] = w2
  sinonimos[w2] = w1
print(sinonimos[input()])