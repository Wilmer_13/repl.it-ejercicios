#Dada una lista de números distintos, intercambie el mínimo y el máximo e imprima la lista resultante.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

lista = list([int(s) for s in input().split()])

max, min = lista.index(max(lista)), lista.index(min(lista))
lista[max], lista[min] = lista[min], lista[max]

print(lista)
