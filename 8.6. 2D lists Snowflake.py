#Dado un número entero positivo impar n, produce una matriz bidimensional de tamaño n × n. Rellene cada elemento con el carácter "." . Luego llene la fila central, la columna central y las diagonales con el carácter "*". Obtendrá una imagen de un copo de nieve. Imprima el copo de nieve en n × n filas y columnas y separe los caracteres con un solo espacio.

# Read an integer:
# a = int(input())
# Print a value:
# print(a)

num = int(input())
a = [['.'] * num for i in range(num)]
for i in range(num):
  for j in range(num):
    if i == j or i + j + 1 == num or i == num // 2 or j == num // 2:
      a[i][j] = '*'
for line in a:
  print(*line)