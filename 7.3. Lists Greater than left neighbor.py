#Dada una lista de números, encuentre e imprima todos sus elementos que sean mayores que su vecino izquierdo.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = [int(s) for s in input().split()]
i = 0

while i < len(a):
  if a[i] > a[i-1] and i > 0:
    print(a[i], end=" ")
  i += 1