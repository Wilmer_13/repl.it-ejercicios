#Dada una lista de enteros distintos de cero, encuentre e imprima el primer par de elementos adyacentes que tengan el mismo signo. Si no existe ese par, imprima 0

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = [int(s) for s in input().split()]
s = []

for i in range(1, len(a)):
  if a[i - 1] > 0 and a[i] > 0:
    s.append(a[i - 1])
    s.append(a[i])
    break
  elif a[i - 1] < 0 and a[i] < 0:
    s.append(a[i - 1])
    s.append(a[i])
    break

if not s:
  print(0)
else:
  print(s)
