#El virus atacó el sistema de archivos de la supercomputadora y rompió el control de los derechos de acceso a los archivos. Para cada archivo hay un conjunto conocido de operaciones que pueden aplicarse a él:

#escribe W,
#leer R,
#ejecutar X.

#La primera línea contiene el número N, el número de archivos contenidos en el sistema de archivos. Las siguientes N líneas contienen los nombres de archivo y las operaciones permitidas con ellos, separadas por espacios. La siguiente línea contiene un número entero M: el número de operaciones para los archivos. En las últimas líneas M, especifique las operaciones que se solicitan para los archivos. Se puede solicitar un archivo muchas veces.

#Debe recuperar el control sobre los derechos de acceso a los archivos. Para cada solicitud, su programa debe devolver OK si la operación solicitada es válida o Acceso denegado si la operación no es válida.

# Read a string:
# s = input()
# Print a value:
# print(s)

n = int(input())
com = {}
for i in range(n):
    myList = list(input().split())
    com[myList[0]] = myList[1:len(myList)]
m = int(input())
for t in range(m):
    opps = list(input().split())
    if 'W' in com[opps[1]] and opps[0] == 'write':
        print('OK')
    elif 'R' in com[opps[1]] and opps[0] == 'read':
        print('OK')
    elif 'X' in com[opps[1]] and opps[0] == 'execute':
        print('OK')
    else:
        print('Access denied')