#Dados dos enteros positivos n y m, cree una matriz bidimensional de tamaño n × m y llénela con los caracteres "." Y "*" en un patrón a cuadros. La esquina superior izquierda debe tener el carácter "." .

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

n, m = [int(s) for s in input().split()]
a = [["."]*m for i in range(n)]
for j in range (n):
    for k in range (m):
        if k%2 != 0 and j%2 == 0:
          a[j][k] = '*'
        elif k%2 == 0 and j%2 != 0:
          a[j][k] = '*'
for line in a:
    print(*line)