#Dada una lista de enteros, encuentre el primer elemento máximo en él. Imprime su valor y su índice (contando con 0).

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

a = [int(s) for s in input().split()]
indice = 0
for i in range(1, len(a)):
    if a[i] > a[indice]:
        indice = i
print(a[indice], indice)