#Dada una lista de números, intercambie elementos adyacentes en cada par (intercambie A [0] con A [1], A [2] con A [3], etc.). Imprime la lista resultante. Si una lista tiene un número impar de elementos, deje el último elemento intacto.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

x = input()
list_x = list(map(int, x.split()))

for x in range (0,len(list_x)-1,2):
  a = list_x.pop(x)
  list_x.insert(x+1 , a)
print(str(list_x).replace('[','').replace(']','').replace(',',''))
