#Dada una lista de números, determine e imprima el número de elementos que son mayores que sus dos vecinos.

#El primero y el último elemento de la lista no deben considerarse porque no tienen dos vecinos.

# Read a list of integers:
# a = [int(s) for s in input().split()]
# Print a value:
# print(a)

x = input()
list_x = list(map(int, x.split()))
contador = 0

for n in range(1,len(list_x)-1):
  if list_x[n-1] < list_x[n] and list_x[n] > list_x[n+1]:
    contador = contador + 1
print(contador)

